Screenshots
===========

Home
----
The "Home" page.
Navigation at the top, then the details of the currently playing track,
then the playlist below (you can only see two songs in the playlist).

Remove items from the playlist by clicking the orange minus sign "-".

Skip to a different part of the playlist by clicking any of the orange "Play" buttons.

![Home](home.png)

Browse
------

Browsing all artists, the first is the "plain" view, the second is the "pretty" view.
The pretty version includes all album cover images, and therefore can take a while to
fully load.

![Plain](artists-plain.png)

![Pretty](artists-pretty.png)

Artist
------

Clicking an artist's name loads a page with albums from that artist only.
This is the pretty version.
The plain version is text only, and arranged as a list.

![Pretty](artist-pretty.png)

Album
-----

Clicking on an album name shows a list of songs on that album.
This is the "plain" view.
The pretty view is very similar; it has the album cover image above the list.

![Plain](album-plain.png)
