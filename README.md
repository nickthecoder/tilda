Tilda Music Player
==================

A simple music player with a web front end.

Written in Kotlin, so it should run on any computer with Java.

![Home](./screenshots/home.png)

[More Screenshots...](./screenshots/index.md)

Yet another music player. Why?
------------------------------

I couldn't find anything that fitted my requirements ;-)

I want to play music through my stereo via a computer, but controllable
from other devices, such as my phone, tablet etc.

I also want my friends and family to use it too, especially on party nights.
I don't want to be the only one choosing the music.

As the interface is a web site, everybody can use Tilda Music Player using
their own phone without installing an app.


Build
-----

You must have java on your computer. All other dependencies will be downloaded
automatically.
This also assumes you have "git", otherwise you'll have to download and unzip it manually.

    git clone https://gitlab.com/nickthecoder/tilda.git
    cd tilda
    ./gradlew

Read the command line usage...

    ./build/install/tilda/bin/tilda --help

If you intend on using the "sox" player (my preference when using a regular linux computer), check that you have
libsox-fmt-pulse installed. Without this, sox will use the alsa plugin, which can cause contension when multiple
applications are playing simultaneously (in particular, I have had problems with MythTV and Sox (using alsa)).

Running
-------

For details on [command line arguments](src/main/kotlin/uk/co/nickthecoder/tilda/Usage.kt) :

    tilda --help

The first time you run tilda, you should specify the filename for the SQLite database
the port number for the web server, the directory (or directories) of your
music library, and also tell it to start a scan of your music.
If you want to enable text-search, then also specify where to store the text-search
meta-data.

For example :

    tilda --db /home/foo/.config/tilda.db --port 8787 --searchDB /home/foo/.config/tildaSearch/ --scan /home/foo/music/

Then point your web browser to [http://localhost:8787]

Thereafter, you can call it without arguments :

    tilda


Features
--------

* Supports mp3, ogg, flac and wav (maybe others but I haven't tried).

* Scans music files from one or more roots, extracting meta-data.
Scanning my collection of 8,500 songs takes about 3 minutes.
It is done in the background, so you can start listening to music straight away.

* Uses a simple very lightweight HTML interface, with no fancy rubbish.
There is a trivial bit of Javascript, which you probably won't miss if you have javascript disabled.

* Designed to be easy to use with fat-fingers on a phone.

* Two views "Pretty" and "Plain".
The pretty view shows album covers.
The plain view is text only, which should work on really old phones!

* An option to switch into random play mode when the playlist is empty.

* "Clever" random play. It only repeats tracks once you have played every song.
Even the music that you picked yourself will NOT be played until every song
has been played.

* Multiple playlists.
Each playlist can be made "permanent", which means the songs are not removed
from the playlist when they have been played.

* Songs or albums can be queued (added to the end of the playlist), or played "now".
"Now" is either immediately, or once the current song has finished playing.
(I get triggered if people stop songs half way through, so I choose the later option).

* NEW Text search. Search for songs/artists/albums (Uses Lucene).

* REST API. Not perfect, but good enough for my needs.
  JSON info at :
  
    /artists
    /artist/<artist-number>
    /albums
    /album/<album-number>
    /song/<song-id>
    
  Use POST requests to perform actions :
  
    /play
    /pause
    /stop
    /skip
    /addAlbum albumId=x play="play" or queue="queue"
    /addSong songId=x play="play" or queue="queue"
    
  

Missing Features
----------------

I may tackle these at some point, but for now, I'm happy with what I've got.

* Cannot re-order a playlist

* Cannot skip forward/backwards through a song

History
-------

This is my second stab at writing this application.
The first was written many years ago in Python, and at some point it stopped working.
Debugging it was annoying, in part because I haven't touched python in such a long time.
(I'm never using Python again, I'm a Kotlin convert ;-)
I worked around the bug, but I wasn't happy with the result.
