REST API
========

This document describes the parameters to each of the web APIs.

All URLs should include the parameter "json" to get back the results as
JSON. Omitting this will return the results as HTML.

Navigation
----------

### /
Displays the home page. There is no JSON version.

### /play (POST)

### /pause (POST)

### /stop (POST)

### /skip (POST)

### /history (GET)

### /scan (POST)

### /state (GET)
Returns the state of the player.
This is no HTML view, only JSON.

Playlists
---------

### /playlist/list (GET)

No parameters

### /playlist/view/<PLAYLIST-ID> (GET)

### /playlist/update/<PLAYLIST-ID> (POST)
name (String), loop (Boolean), permanent (Boolean)

### /playlist/delete/<PLAYLIST-ID> (POST)

### /playlist/select/<PLAYLIST-ID> (POST)

### /playlist/clear/<PLAYLIST-ID> (POST)

### /playlist/new (POST)
This currently does NOT return the new playlist ID, nor
allow the values to be set. As such it is only useable via the GUI.

### /playlist/addSong (POST)
Adds a song to the current playlist.

songId (Int), queue (Boolean)

### /playlist/addAlbum (POST)
Adds an album to the current playlist.

albumId (Int), queue(Boolean), clear (Boolean)

### /playlist/playEntry (POST)
Plays a song which is already in the current playlist.

entryId (Int)

### /playlist/removeEntry (POST)

entryId (Int)

Artists
-------

### /artist/list (GET)
Lists all artists

### /artist/view/<ARTIST-ID> (GET)

Albums
------

### /album/list/<ALBUM-ID> (GET)

### /albumCover/view/<ALBUM-ID> (GET)

Songs
-----

### /song/view/<SONG-ID> (GET)

Search
------

### /search (GET)
Search song names, album names and artist names. Only whole words are searched.
Uses Lucene to perform the search.
Note: this is a GET request.

q (String) - The query string for the search.

Settings
--------

### /settings/list (GET)

### /settings/update (POST)
Updates a boolean setting. The value is changed to true iff the "set"
parameter is present.

settingId (Int), set(Boolean)
