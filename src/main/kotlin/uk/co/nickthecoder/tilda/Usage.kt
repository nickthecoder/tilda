package uk.co.nickthecoder.tilda

val usage = """Usage :

tilda [--db databaseFile] [--port PORT_NUMBER] [--searchDB DIRECTORY] [OTHER_OPTIONS...] [musicDirectory...]")

    --db databaseFile
        The filename of the database. If this is omitted, the previously used database
        will be used. If this is the first time tilda is run, then "tilda.db" is the default.

        Stored in the "registry", and therefore it can be omitted on subsequent runs.

    --port portNumber
        The port number of the web server. The default is 8787.
        Port 80 is the standard http port, but you need to be root to use that port.

        This value is stored in the database, and can therefore be omitted on subsequent runs.

    --searchDB DIRECTORY
        To enable searching, you must specify a directory to hold the search meta-data.
        If the directory does not exist, it will be created.

        This value is stored in the database, and can therefore be omitted on subsequent runs.
        If you enable search, but using this option, and then change your mind, you can disable it
        again by passing an empty string :
            tilda --searchDB ""

    musicDirectory...
        The directory where you keep your music.
        You can specify more than one directory if you want.

        Stored in the database, and can therefore be omitted on subsequent runs.


Other Options :


    --deletePlaylists
        Deletes all playlists. A new "default" playlist will be created.
        Note, you can delete playlists from the web frontend (except the "current" playlist).

    --scan
        Scans the music directory (or directories), extracting the meta-data into the database.
        You MUST scan the first time you use tilda, and then scan again whenever you want to
        include new music (or to remove deleted music).

    --purge
        Similar to --scan, but also permanently deletes all meta-data for songs/albums/artists
        that no long exist in the music directory (or directories).
        In comparison, --scan merely tags those entries as "unavailable", but does not delete them.

    --rebuild
        Rebuilds the database. If you update tilda, you may need to rebuild the database.
        Settings are copied over to the new schema.
        A scan is done to re-populate songs/albums/artists.
        Playlists will be lost.
        The lucene data will also be rebuilt (if you have enabled search, see --searchDB DIRECTORY).

    --rebuildSearch
        Rebuilds the search meta-data. See --searchDB DIRECTORY.
        Hopefully, this won't be needed. But if the lucene database gets corrupted..

        NOTE. If you manually delete the search directory (passed to --searchDB),
        and re-run tilda, then search database will be repopulated.
        Which makes this option all the more pointless ;-)

    --play / --no-play
        Should playback start immediately? If both are omitted, then the previous settings
        is used.

    --skin SKIN_NAME
        Change to a different skin. Currently, SKIN_NAME can be "default" or "alternate".

        To create your own skin, add the css file and images into a jar file placed in
        the "lib" directory.
        The package name should be in the form : co.uk.nickthecoder.tilda.skins.YOUR_SKIN_NAME
        or set SKIN_NAME to the fully qualified package name.

    --player default | omx | sox
        Changes how sound is played.
        
        default
            Uses Java's sound APIs, and should run on any system,
            but may not be fast enough on low spec machines.
            Also, Java's sound API are not included in the headless jres.
            On linux, Java's sound API also requires pulse audio I believe. Grr.
            
        omx
            Only available for the Raspberry Pi
            Uses the omxplayer command. Stop, pause and unpause are implemented by sending
            'q', and 'p' to the processes standard input.

        sox
            Only available on unix-like system (i.e. everything except windows!)
            Uses the "play" command, which is part of the "sox" package.
            Note, you will also need the appropriate sox libraries for each
            filetype. Many linux distributions do not include the mp3 library
            by default due to patent restrictions of mp3.
            On debian systems, install libsox-fmt-all
        
    --showQuit
    --hideQuit
        The settings page can optionally have a "Quit" button.
        Despite the name, the quit functionality is also *disabled* if you use --hideQuit.
        
    --stop
        Sends a stop message to a running instance. Can not be used if tilda was started using
        the --hideQuit option.
        You must have the command line program "curl" installed.
        
    --debug
        Turns on debugging messages which are normally not displayed.

    --silent
        Turns off all logging message.
        Note : error message (e.g. due to incorrect command line arguments) are still displayed.

    --help
        Prints this message

Examples :

    For the first time, define the database's filename, the port number, the text search directory,
    and the location of the music :

    tilda --db ~/.config/tildaDataBase.db --port 8888 --searchDB  ~/.config/tildaSearch/ ~/Music

    Now point your browser at http://localhost:8888


    Subsequently, you can call tilda without arguments :

    tilda

    And once again, point your browser at http://localhost:8888

"""