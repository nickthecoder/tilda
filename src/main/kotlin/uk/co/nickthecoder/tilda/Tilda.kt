package uk.co.nickthecoder.tilda

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.impl.SimpleLogger
import uk.co.nickthecoder.tilda.model.*
import uk.co.nickthecoder.tilda.player.OmxPlayer
import uk.co.nickthecoder.tilda.player.SoxPlayer
import uk.co.nickthecoder.tilda.player.player
import java.io.File
import java.sql.Connection
import java.util.logging.ConsoleHandler
import java.util.logging.Level
import java.util.logging.Logger
import java.util.prefs.Preferences


object Tilda {

    val logger = Logger.getLogger(Tilda.javaClass.`package`.name)

    @JvmStatic
    fun connect(file: File) {
        Database.connect("jdbc:sqlite:${file.path}", "org.sqlite.JDBC")
        TransactionManager.manager.defaultIsolationLevel =
            Connection.TRANSACTION_SERIALIZABLE // Or Connection.TRANSACTION_READ_UNCOMMITTED
    }

    /**
     * Used during development.
     */
    private fun deletePlaylist() {
        transaction { SchemaUtils.drop(Playlists, Entries) }
    }

    @JvmStatic
    fun main(args: Array<String>) {

        // Used to store the location of the database. All other settings are stored in the database.
        val prefs = Preferences.userNodeForPackage(Tilda.javaClass)

        // Default values
        var db = prefs.get("db", "tilda.db")
        var lucene: String? = null
        var port: Int? = null
        val path = mutableListOf<String>()
        var scan = false
        var purge = false
        var rebuild = false
        var rebuildSearch = false
        var skin: String? = null
        var deletePlaylists = false
        var play: Boolean? = null
        var debug = false
        var silent = false
        var playerStr: String? = null
        var showQuit: Boolean? = null
        var stop = false

        var endOptions = false
        var next: String? = null

        // Parse command line arguments
        for (arg in args) {
            if (next == null) {

                if (endOptions) {
                    path.add(arg)
                } else {
                    when (arg) {
                        "--" -> endOptions = true
                        "--deletePlaylists" -> deletePlaylists = true
                        "--scan" -> scan = true
                        "--purge" -> purge = true
                        "--rebuild" -> rebuild = true
                        "--rebuildSearch" -> rebuildSearch = true
                        "--port" -> next = "PORT"
                        "--play" -> play = true
                        "--no-play" -> play = false
                        "--skin" -> next = "SKIN"
                        "--db" -> next = "DB"
                        "--searchDB" -> next = "SEARCH_DB"
                        "--debug" -> debug = true
                        "--silent" -> silent = true
                        "--player" -> next = "PLAYER"
                        "--showQuit" -> showQuit = true
                        "--hideQuit" -> showQuit = false
                        "--stop" -> stop = true
                        "-h" -> {
                            println(usage)
                            return
                        }
                        "--help" -> {
                            println(usage)
                            return
                        }
                        else -> {
                            if (arg.startsWith("-")) {
                                println("Unknown option : $arg")
                                return
                            } else {
                                path.add(arg)
                            }
                        }
                    }
                }
            } else {
                when (next) {
                    "PORT" -> port = arg.toInt()
                    "DB" -> db = arg
                    "SKIN" -> skin = arg
                    "SEARCH_DB" -> lucene = arg
                    "PLAYER" -> playerStr = arg
                }
                next = null
            }

        }

        if (debug) {
            logger.level = Level.ALL
            logger.addHandler(ConsoleHandler().apply { level = Level.ALL })
            logger.fine("Debug enabled")
        }

        if (silent) {
            logger.level = Level.OFF
            System.setProperty(SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "warn")
        }

        prefs.put("db", File(db).absolutePath)

        if (playerStr == null) {
            playerStr = prefs.get("player", "default")
        }

        println( "Player : ${playerStr}" )
        when (playerStr) {
            "omx" -> {
                logger.log(Level.INFO, "Using OmxPlayer")
                player = OmxPlayer()
            }
            "sox" -> {
                logger.log(Level.INFO, "Using SoxPlayer")
                player = SoxPlayer()
            }
            else -> {
                playerStr = "default"
                logger.log(Level.INFO, "Using default Player")
            }
        }
        prefs.put("player", playerStr)
        prefs.flush()

        logger.log(Level.INFO, "Connecting to database : $db")
        connect(File(db))

        if (rebuild) {
            Model.rebuildDatabase()
        } else {
            if (deletePlaylists) deletePlaylist()
            Model.createSchema()
        }

        // Update the lucene directory location
        if (lucene != null) {
            transaction {
                KnownSettings.LUCENE_DIRECTORY.find()?.let {
                    if (lucene.isBlank()) {
                        it.value = ""
                    } else {
                        it.value = File(lucene).absolutePath
                    }
                }
            }
        }

        // Update the music path from the command line
        if (path.isNotEmpty()) {
            path.forEach {
                if (!File(it).exists()) {
                    println("Directory $it does not exist")
                    return
                }
            }
            transaction {
                KnownSettings.MUSIC_PATH.find()?.let {
                    it.value = path.joinToString(separator = File.pathSeparator)
                }
            }
        }

        // Update the skin?
        if (skin != null) {
            transaction {
                KnownSettings.SKIN?.find()?.value = skin
            }
        }

        // Update the showQuit
        println("Updating show quit to $showQuit")
        if (showQuit != null) {
            transaction {
                KnownSettings.SHOW_QUIT?.find()?.booleanValue = showQuit
            }
        }

        Controller.begin()

        // Update the PLAY_ON_STARTUP setting?
        if (play != null) {
            transaction {
                KnownSettings.PLAY_ON_STARTUP?.find()?.booleanValue = play
            }
        }

        // Update the port number from the command line
        if (port != null) {
            transaction {
                KnownSettings.SERVER_PORT.find()?.let {
                    it.intValue = port
                }
            }
        }

        if (rebuildSearch) {
            Controller.search?.rebuild()
        }

        if (rebuild || scan || purge) Scanner.scan(purge)

        // Start playing?
        transaction {
            if (KnownSettings.PLAY_ON_STARTUP.isTrue()) {
                val playlist = Playlist.findCurrent()
                playlist.entries.firstOrNull { it.orderNumber == 1 }?.let {
                    playlist.position = 1
                    player.play(it.song.file)
                }
            }
        }

        if (stop) {
            transaction {
                if (!KnownSettings.SHOW_QUIT.isTrue()) {
                    println("Tilda wasn't started with the --showStop option, and therefore we cannot send a stop message.")
                    System.exit(-1)
                }
            }
            val portNumber = port ?: catchingTransaction {
                KnownSettings.SERVER_PORT.find()?.intValue
            } ?: 8787
            ProcessBuilder().command("curl", "-X", "POST", "-F", "quit=quit", "http://localhost:${portNumber}/quit")
                .start()
        } else {
            WebServer.createEngine().start(wait = true)
        }

    }
}

fun humanTime(seconds: Int): String {
    val minutes = seconds / 60
    val secs = seconds - minutes * 60

    return String.format("%d:%02d", minutes, secs)
}
