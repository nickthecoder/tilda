package uk.co.nickthecoder.tilda

import io.ktor.application.call
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.request.receiveParameters
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import org.jetbrains.exposed.sql.transactions.transaction
import uk.co.nickthecoder.tilda.model.KnownSettings
import uk.co.nickthecoder.tilda.model.Setting
import uk.co.nickthecoder.tilda.player.player

internal fun Route.routeSettings() {

    get("list") {
        val (settings, showQuit) = transaction {
            Pair(
                Setting.all().associateBy({ it.id.value }, { it }),
                KnownSettings.SHOW_QUIT?.find()?.booleanValue ?: false
            )
        }
        call.respondFreemarker(
            FreeMarkerContent(
                "settings.ftlh",
                mapOf(
                    "settings" to settings,
                    "player" to player,
                    "scanner" to Scanner,
                    "search" to Controller.search,
                    "showQuit" to showQuit
                ),
                "e"
            )
        )
    }

    post("update") {
        val parameters = call.receiveParameters()
        call.requiredStringParameter("settingId", parameters)?.let { settingsId ->
            transaction {
                Setting.findById(settingsId)?.let {
                    it.booleanValue = parameters["set"] != null
                }
            }
            call.respondOk(ViewType.NORMAL)
        }
    }
}
