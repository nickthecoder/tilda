package uk.co.nickthecoder.tilda

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.routing.Route
import io.ktor.routing.get
import uk.co.nickthecoder.tilda.model.Artist
import uk.co.nickthecoder.tilda.player.player

internal fun Route.routeArtist() {
    get("list") {
        call.artists(call.viewType())
    }
    get("view/{artistId}") {
        call.requiredIntParameter("artistId")?.let { artistId ->
            call.artist(call.viewType(), artistId)
        }
    }
}

internal suspend fun ApplicationCall.artists(viewType: ViewType) {
    findOrError(viewType, "Artists") {
        val artists = Artist.all().filter { it.available && it.name.isNotBlank() }.sortedBy { it.name }
        if (viewType == ViewType.PRETTY) {
            artists.forEach { artist ->
                artist.lazyAlbums
            }
        }
        artists

    }?.let { artists ->

        val grouped = artists.groupBy { it.name.substring(0, 1) }
        if (viewType == ViewType.JSON) {
            respondJson(artists.map { artist ->
                mapOf(
                        "artistId" to artist.id.value,
                        "artistName" to artist.name
                )
            })
        } else {
            val fm = FreeMarkerContent(if (viewType == ViewType.PRETTY) "artists-pretty.ftlh" else "artists.ftlh", mapOf(
                    "grouped" to grouped,
                    "artists" to artists, // No longer needed???
                    "player" to player,
                    "search" to Controller.search
            ), "e")

            respondFreemarker(fm)
        }
    }
}

internal suspend fun ApplicationCall.artist(viewType: ViewType, artistId: Int) {
    findOrError(viewType, "Artist") {
        Artist.findById(artistId)?.apply {
            lazyAlbums
        }
    }?.let { artist ->

        if (viewType == ViewType.JSON) {
            respondJson(
                    mapOf<String, Any>(
                            "artistId" to artist.id.value,
                            "artistName" to artist.name,

                            "albums" to artist.albums.map { album ->
                                mapOf<String, Any>(
                                        "albumId" to album.id.value,
                                        "albumName" to album.name,

                                        "songs" to album.songs.map { song ->
                                            mapOf<String, Any>(
                                                    "songId" to song.id.value,
                                                    "songName" to song.name,
                                                    "trackNumber" to song.trackNumber,
                                                    "seconds" to song.seconds,
                                                    "path" to song.path
                                            )
                                        }
                                )
                            }
                    )
            )

        } else {
            val fm = FreeMarkerContent(if (viewType == ViewType.PRETTY) "artist-pretty.ftlh" else "artist.ftlh", mapOf(
                    "artist" to artist,
                    "player" to player,
                    "search" to Controller.search
            ), "e")
            respondFreemarker(fm)
        }
    }
}

