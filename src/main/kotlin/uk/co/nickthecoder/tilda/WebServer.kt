package uk.co.nickthecoder.tilda

import com.fasterxml.jackson.core.util.DefaultIndenter
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter
import com.fasterxml.jackson.databind.SerializationFeature
import freemarker.cache.ClassTemplateLoader
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.*
import io.ktor.freemarker.FreeMarker
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.jackson.jackson
import io.ktor.request.receiveParameters
import io.ktor.response.cacheControl
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.server.engine.*
import io.ktor.server.netty.Netty
import org.jetbrains.exposed.sql.SortOrder
import org.jetbrains.exposed.sql.transactions.transaction
import uk.co.nickthecoder.tilda.model.KnownSettings
import uk.co.nickthecoder.tilda.model.Playlist
import uk.co.nickthecoder.tilda.model.Song
import uk.co.nickthecoder.tilda.model.Songs
import uk.co.nickthecoder.tilda.player.player


object WebServer {

    private fun port() = catchingTransaction {
        KnownSettings.SERVER_PORT.find()?.intValue
    } ?: 8787

    private fun showQuit() = transaction { KnownSettings.SHOW_QUIT.find()?.booleanValue }

    fun createEngine(): ApplicationEngine {

        val skinName = transaction { KnownSettings.SKIN.find()?.value ?: "default" }
        val skinPackage = if (skinName.contains(".")) skinName else Tilda.javaClass.`package`.name + ".skins.$skinName"

        return embeddedServer(Netty, port()) {

            if (showQuit() == true) {
                install(ShutDownUrl.ApplicationCallFeature) {
                    // The URL that will be intercepted
                    shutDownUrl = "/quit"
                    // A function that will be executed to get the exit code of the process
                    exitCodeSupplier = {
                        player.stop()
                        0
                    }
                }
            }

            install(CachingHeaders) {
                options { outgoingContent ->
                    when (outgoingContent.contentType?.withoutParameters()) {
                        ContentType.Text.JavaScript,
                        ContentType.Application.JavaScript,
                        ContentType.Text.CSS,
                        ContentType.Image.PNG,
                        ContentType.Image.Any -> CachingOptions(CacheControl.MaxAge(maxAgeSeconds = 36000))
                        else -> null
                    }
                }
            }
            install(DefaultHeaders)
            install(Compression)
            install(CallLogging)
            install(FreeMarker) {
                templateLoader = ClassTemplateLoader(Tilda.javaClass, "templates")
                defaultEncoding = "UTF-8"
                numberFormat = "computer"
                setAutoImports(mapOf("layout" to "layout.ftlh"))
            }

            install(ContentNegotiation) {
                jackson {
                    configure(SerializationFeature.INDENT_OUTPUT, true)
                    setDefaultPrettyPrinter(DefaultPrettyPrinter().apply {
                        indentArraysWith(DefaultPrettyPrinter.FixedSpaceIndenter.instance)
                        indentObjectsWith(DefaultIndenter("  ", "\n"))
                    })
                }
            }
            routing {
                static("js") {
                    resources(Tilda.javaClass.`package`.name + ".js")
                }
                static("skin") {
                    resources(skinPackage)
                }

                route("/song") {
                    routeSong()
                }
                route("/playlist") {
                    routePlaylist()
                }

                route("/artist") {
                    routeArtist()
                }

                route("/album") {
                    routeAlbum()
                }

                route("/settings") {
                    routeSettings()
                }

                get("/") {
                    val result = catchingTransaction {
                        val playlist = Playlist.findCurrent()
                        var currentSong: Song? = null
                        playlist.lazyEntries.forEach { entry ->
                            entry.lazySong.lazyAlbum.lazyArtist
                            if (entry.orderNumber == playlist.position) {
                                currentSong = entry.lazySong
                            }
                        }

                        FreeMarkerContent(
                            "home.ftlh", mapOf(
                                "playlist" to playlist,
                                "currentSong" to currentSong,
                                "player" to player,
                                "search" to Controller.search
                            ), "e"
                        )
                    }
                    // Never cache the home page.
                    // Hitting "back" still uses the browser cache though, so I had to use javascript to reload the page.
                    call.response.cacheControl(CacheControl.MaxAge(0, 0, true, true))
                    call.respondFreemarker(result)
                }


                post("/play") {
                    val parameters = call.receiveParameters()
                    transaction { Controller.play() }
                    call.respondOk(call.viewType(parameters))
                }
                post("/pause") {
                    val parameters = call.receiveParameters()
                    player.pause()
                    call.respondOk(call.viewType(parameters))
                }
                post("/stop") {
                    val parameters = call.receiveParameters()
                    player.stop()
                    call.respondOk(call.viewType(parameters))
                }
                post("/skip") {
                    val parameters = call.receiveParameters()
                    transaction { Controller.skip() }
                    call.respondOk(call.viewType(parameters))
                }

                get("/history") {
                    val result = catchingTransaction {
                        val history = Song.find { Songs.lastPlayed.isNotNull() }
                            .orderBy(Pair(Songs.lastPlayed, SortOrder.DESC))
                            .limit(100).toList()
                        history.forEach { it.lazyAlbum.lazyArtist }

                        FreeMarkerContent(
                            "history.ftlh", mapOf(
                                "history" to history,
                                "player" to player,
                                "scanner" to Scanner,
                                "search" to Controller.search
                            ), "e"
                        )
                    }
                    call.respondFreemarker(result)
                }

                post("/scan") {
                    Scanner.scan(false)
                    call.respondOk(ViewType.NORMAL)
                }

                get("/state") {
                    call.respondJson(
                        catchingTransaction {
                            val state = player.playerState
                            val seconds = player.seconds
                            val entry = Controller.currentEntry()
                            val song = entry?.lazySong
                            val album = song?.album
                            val artist = album?.artist
                            val playlist = entry?.lazyPlaylist ?: Playlist.findCurrent()

                            mapOf<String, Any?>(
                                "state" to state.name,
                                "seconds" to seconds,
                                "songId" to song?.id?.value,
                                "songName" to song?.name,
                                "songFile" to song?.file,
                                "songSeconds" to song?.seconds,
                                "albumId" to album?.id?.value,
                                "albumName" to album?.name,
                                "artistId" to artist?.id?.value,
                                "artistName" to artist?.name,
                                "playlistId" to playlist.id.value,
                                "playListName" to playlist.name
                            )
                        }
                    )
                }


                get("/search") {
                    val searchString = call.parameters["q"] ?: ""
                    val results = Controller.search?.search(searchString, 20)
                    val content = FreeMarkerContent(
                        "search.ftlh", mapOf(
                            "results" to results,
                            "player" to player,
                            "search" to Controller.search,
                            "query" to searchString
                        ), "e"
                    )

                    call.respondFreemarker(content)
                }
            }
        }
    }

}
