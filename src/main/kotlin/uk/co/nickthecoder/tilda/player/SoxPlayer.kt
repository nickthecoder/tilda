package uk.co.nickthecoder.tilda.player

import uk.co.nickthecoder.tilda.Tilda
import java.io.File
import java.util.logging.Level

class SoxPlayer : ProcessPlayer() {

    override fun processBuilder(file: File) : ProcessBuilder {
        Tilda.logger.log( Level.INFO, "Playing using sox : ${file.absolutePath}" )
        return ProcessBuilder("play", "-q", file.absolutePath)
    }

    override fun nonZeroExitStatus(exitStatus: Int) {
        super.nonZeroExitStatus(exitStatus)
        // The most common cause of problems with sox is not having the appropriate libraries
        // installed for the file type being played.
        if (exitStatus == 2) {
            Tilda.logger.log(Level.WARNING, "Ensure that you have installed libsox-fmt-all")
        }
    }
}
