package uk.co.nickthecoder.tilda.player

import uk.co.nickthecoder.tilda.Tilda
import uk.co.nickthecoder.tilda.sigcont
import uk.co.nickthecoder.tilda.sigint
import uk.co.nickthecoder.tilda.sigstop
import uk.co.nickthecoder.tilda.sigkill
import java.io.File
import java.util.*
import java.util.logging.Level
import java.util.concurrent.TimeUnit


/**
 * Spawns a new process running a command, such as "play".
 */
abstract class ProcessPlayer : AbstractPlayer() {

    private var process: Process? = null

    protected abstract fun processBuilder(file: File): ProcessBuilder

    protected open fun nonZeroExitStatus(exitStatus: Int) {
        if (exitStatus != 129) { // Killed processes return this???
            Tilda.logger.log(Level.WARNING, "${javaClass.simpleName}'s exit status : $exitStatus")
        }
    }

    override fun play(file: File) {
        if (isPlaying()) {
            stop()
        }

        state = PlayerState.PLAYING
        startTime = Date().time
        mutableLatestFile = file

        val pb = processBuilder(file)
        process = pb.start()

        val currentProcess = process
        if (currentProcess != null) {
            Thread {
                val exitStatus = currentProcess.waitFor()
                if (exitStatus != 0) {
                    nonZeroExitStatus(exitStatus)
                }
                Tilda.logger.log(Level.FINE, "Song finished (or was stopped). $state")

                if (process == currentProcess) {
                    process = null
                    if (state == PlayerState.PLAYING) {
                        Tilda.logger.log(Level.FINE, "Changing to IDLE")
                        state = PlayerState.IDLE
                    } else if (state == PlayerState.STOPPING) {
                        Tilda.logger.log(Level.FINE, "Changing to STOPPED")
                        state = PlayerState.STOPPED
                    } else {
                        Tilda.logger.log(Level.FINE, "Not changing state from $state")
                    }
                } else {
                    Tilda.logger.log(Level.FINE, "Not changing state, as the current process isn't the same. Null ? ${process === null}")
                }
                Tilda.logger.log(Level.FINE, "Now state is $state")

            }.start()
        }
    }

    override fun stop() {
        if (state == PlayerState.PLAYING || state == PlayerState.PAUSED) {

            process?.let { currentProcess ->
                Tilda.logger.log(Level.FINE, "Stopping (by sending SIGINT)")
                state = PlayerState.STOPPING
                currentProcess.sigint()
                
                if (currentProcess.isAlive() ) {
                    // Give it 2 seconds to stop, but if that fails, then forcably kill it
                    currentProcess.waitFor(2,TimeUnit.SECONDS)
                    currentProcess.sigkill()
                }
                if (currentProcess.isAlive() ) {
                    process?.waitFor(2,TimeUnit.SECONDS)
                }
            }
        }
    }

    override fun pause() {
        if (state == PlayerState.PLAYING) {
            Tilda.logger.log(Level.FINE, "Pausing (by sending a SIGSTOP)")
            state = PlayerState.PAUSED
            process?.sigstop()
        }
    }

    override fun unPause() {
        if (state == PlayerState.PAUSED) {
            Tilda.logger.log(Level.FINE, "Playing (by sending a SIGCONT)")
            state = PlayerState.PLAYING
            process?.sigcont()
        }
    }
}
