package uk.co.nickthecoder.tilda.player


import uk.co.nickthecoder.tilda.humanTime
import java.io.File
import java.util.*

/**
 * IDLE - When a track ends normally. This will cause the Controller to play the next track in the playlist.
 *
 * STOPPING - An intermediate state after PLAYING, until the Streaming Thread has finished.
 *
 * STOPPED - Stopped by the user. Unlike IDLE, the next item in the playlist will NOT be played.
 *
 * PLAYING - Fairly obvious!
 *
 * PAUSED - The Streaming Thread is still active, but doing nothing. It is slightly wasteful of resources
 * to stay paused for long periods. Ideally, after a set period we should move from PAUSED to STOPPED.
 */
enum class PlayerState { STOPPED, PLAYING, IDLE, PAUSED, STOPPING }

interface Player {

    val playerState: PlayerState

    val latestFile: File?

    val seconds: Int

    val time
        get() = humanTime(seconds)


    fun isPlaying() = playerState == PlayerState.PLAYING

    fun isPaused() = playerState == PlayerState.PAUSED

    fun isStopped() = playerState == PlayerState.STOPPED || playerState == PlayerState.IDLE

    fun play(file: File)

    fun stop()

    fun pause()

    fun unPause()

    fun addListener(listener: PlayerListener)
    fun removeListener(listener: PlayerListener)
}

var player: Player = DefaultPlayer()

abstract class AbstractPlayer : Player {

    protected val listeners = mutableListOf<PlayerListener>()

    protected var startTime: Long = Date().time

    protected var pausedTime: Long = startTime

    protected var mutableLatestFile: File? = null

    override val latestFile: File?
        get() = mutableLatestFile

    override val seconds: Int
        get() {
            return if (state == PlayerState.PAUSED)
                ((pausedTime - startTime) / 1000).toInt()
            else
                ((Date().time - startTime) / 1000).toInt()
        }

    override val playerState
        get() = state

    protected var state = PlayerState.STOPPED
        set(v) {
            if (field != v) {
                if (v == PlayerState.PAUSED) {
                    pausedTime = Date().time
                } else if (v == PlayerState.PLAYING && field == PlayerState.PAUSED) {
                    startTime += (Date().time - pausedTime)
                }

                val old = field
                field = v
                listeners.forEach { listener ->
                    try {
                        listener.stateChanged(old, v)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }

    override fun addListener(listener: PlayerListener) {
        listeners.add(listener)
    }

    override fun removeListener(listener: PlayerListener) {
        listeners.remove(listener)
    }
}
