package uk.co.nickthecoder.tilda.player

import java.io.File
import java.util.*
import javax.sound.sampled.*
import java.util.logging.Level
import uk.co.nickthecoder.tilda.Tilda


class DefaultPlayer : AbstractPlayer() {

    private var streamThread: Thread? = null

    private var inputStream: AudioInputStream? = null

    override fun play(file: File) {
	    Tilda.logger.log( Level.INFO, "DefaultPlayer.play Playing ${file}" )

        if (isPlaying()) {
            stop()
        }

        inputStream = AudioSystem.getAudioInputStream(file)
        mutableLatestFile = file

        inputStream?.let { inputStream ->

            state = PlayerState.PLAYING
            startTime = Date().time

            val outFormat = getOutFormat(inputStream.format)
            val info = DataLine.Info(SourceDataLine::class.java, outFormat)
            val line = AudioSystem.getLine(info) as SourceDataLine

            line.open(outFormat)
            line.start()
            val converted = AudioSystem.getAudioInputStream(outFormat, inputStream)
            streamThread = Thread { stream(converted, line) }.apply {
                // priority = Thread.MAX_PRIORITY
                start()
            }

        }
    }

    override fun stop() {
        if (state == PlayerState.PLAYING || state == PlayerState.PAUSED) {
            state = PlayerState.STOPPING

            while (streamThread != null && state != PlayerState.IDLE) {
                //streamThread?.interrupt()
                streamThread?.join()
            }
        }
    }

    override fun pause() {
        if (state == PlayerState.PLAYING) {
            state = PlayerState.PAUSED
        }
    }

    override fun unPause() {
        if (state == PlayerState.PAUSED) {
            state = PlayerState.PLAYING
        }
    }

    private fun getOutFormat(inFormat: AudioFormat): AudioFormat {
        val ch = inFormat.channels
        val rate = inFormat.sampleRate
        return AudioFormat(AudioFormat.Encoding.PCM_SIGNED, rate, 16, ch, ch * 2, rate, false)
    }

    private fun stream(input: AudioInputStream, line: SourceDataLine) {
        try {
            val buffer = ByteArray(65536)
            var n = 0
            while (n != -1 && state != PlayerState.STOPPING) {
                if (state == PlayerState.PLAYING) {
                    if ( n > 0 ) {
                        line.write(buffer, 0, n)
                    }
                    n = input.read(buffer, 0, buffer.size)
                } else {
                    Thread.sleep(1000)
                }
            }
        } finally {
            line.drain()
            line.stop()
            inputStream?.close()
            inputStream = null
            streamThread = null

            if (state == PlayerState.STOPPING) {
                state = PlayerState.STOPPED
            } else {
                state = PlayerState.IDLE
            }
        }
    }

}
