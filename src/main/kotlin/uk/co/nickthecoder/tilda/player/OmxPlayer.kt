package uk.co.nickthecoder.tilda.player

import uk.co.nickthecoder.tilda.NULL_FILE
import uk.co.nickthecoder.tilda.Tilda
import java.io.File
import java.util.logging.Level
import java.util.logging.Logger


/**
 * This uses the omxplayer command, which is a media player for the Raspberry Pi.
 * https://github.com/popcornmix/omxplayer
 *
 * Therefore this player is only useful on a Raspberry Pi. FYI, the DefaultPlayer
 * doesn't work on the Raspberry Pi ;-)
 *
 * Spawns a new process running the omxplayer command.
 * To pause, unpause a "p" is sent to the process's input stream.
 * To stop playback a "q" is sent to the process's input stream.
 */
class OmxPlayer : ProcessPlayer() {

    private val logger = Logger.getLogger(Tilda.javaClass.`package`.name)

    private var process: Process? = null

    override fun processBuilder(file : File) : ProcessBuilder {
            Tilda.logger.log( Level.INFO, "OmxPlayer.play $file" )
            return ProcessBuilder( "omxplayer", file.absolutePath).apply {
                redirectOutput(NULL_FILE)
                redirectError(NULL_FILE)
            }
    }

    override fun stop() {
        if (state == PlayerState.PLAYING || state == PlayerState.PAUSED) {
            logger.log( Level.FINE, "Stopping (by sending a q character)" )
            state = PlayerState.STOPPING
            process?.outputStream?.let {
                it.write('q'.toInt())
                it.flush()
                process?.waitFor()
            }
        }
    }

    override fun pause() {
        if (state == PlayerState.PLAYING) {
            logger.log( Level.FINE, "Pausing (by sending a p character)" )
            state = PlayerState.PAUSED
            process?.outputStream?.let {
                it.write('p'.toInt())
                it.flush()
            }
        }
    }

    override fun unPause() {
        if (state == PlayerState.PAUSED) {
            logger.log( Level.FINE, "Playing (by sending a p character)" )
            state = PlayerState.PLAYING
            process?.outputStream?.let {
                it.write('p'.toInt())
                it.flush()
            }
        }
    }
}
