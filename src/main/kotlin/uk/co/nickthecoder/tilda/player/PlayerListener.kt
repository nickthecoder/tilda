package uk.co.nickthecoder.tilda.player

interface PlayerListener {

    fun stateChanged(oldState: PlayerState, newState: PlayerState)

}
