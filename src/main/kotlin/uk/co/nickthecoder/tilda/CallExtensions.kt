package uk.co.nickthecoder.tilda

import io.ktor.application.ApplicationCall
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.http.HttpStatusCode
import io.ktor.http.Parameters
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import org.jetbrains.exposed.sql.transactions.transaction
import uk.co.nickthecoder.tilda.player.player

internal suspend fun ApplicationCall.viewType(parameters: Parameters = this.parameters): ViewType {
    if (parameters["json"] != null) return ViewType.JSON
    if (parameters["pretty"] != null) return ViewType.PRETTY
    return ViewType.NORMAL
}

internal suspend fun ApplicationCall.requiredIntParameter(parameterName: String, parameters: Parameters = this.parameters): Int? {
    val result = try {
        parameters[parameterName]?.toInt()
    } catch (e: Exception) {
        null
    }

    if (result == null) {
        respond(HttpStatusCode.BadRequest, "Invalid/missing integer parameter $parameterName")
    }
    return result
}

internal suspend fun ApplicationCall.requiredStringParameter(parameterName: String, parameters: Parameters = this.parameters): String? {
    val result = try {
        parameters[parameterName]
    } catch (e: Exception) {
        null
    }

    if (result == null) {
        respond(HttpStatusCode.BadRequest, "Invalid/missing integer parameter $parameterName")
    }
    return result
}

internal suspend fun ApplicationCall.respondJson(result: Any?) {
    if (result == null) {
        respond(HttpStatusCode.NotFound, "Not found")
    } else {
        respond(result)
    }
}

internal suspend fun ApplicationCall.respondError(viewType: ViewType, message: String, statusCode: HttpStatusCode) {
    if (viewType == ViewType.JSON) {
        respond(HttpStatusCode.BadRequest, message)
    } else {
        val fm = FreeMarkerContent(
                "error.ftlh",
                mapOf(
                        "message" to message,
                        "player" to player,
                        "search" to Controller.search
                )
        )
        respond(statusCode, fm)
    }
}

internal suspend fun ApplicationCall.respondFreemarker(result: FreeMarkerContent?) {
    if (result == null) {
        respond(HttpStatusCode.NotFound, "Not found")
    } else {
        respond(result)
    }
}

internal suspend fun ApplicationCall.respondOk(viewType: ViewType) {
    if (viewType == ViewType.JSON) {
        respondJson(mapOf("ok" to "ok"))
    } else {
        val ref = request.headers["Referer"]
        respondRedirect(ref ?: "/")
    }
}

/**
 * Get row(s) from the database.
 * Reports a error using the "error.ftlh" template if the transaction
 * fails, or returns no rows. (two different messages).
 *
 * @return null if an error occurred or no record was found
 *  otherwise the result of the transaction.
 */
internal suspend fun <T> ApplicationCall.findOrError(viewType: ViewType, name: String, action: () -> T?): T? {
    return try {
        val result = transaction {
            action()
        }
        if (result == null) {
            respondError(viewType, "$name not found", HttpStatusCode.NotFound)
        }
        result
    } catch (e: Exception) {
        respondError(viewType, "Error", HttpStatusCode.BadRequest)
        null
    }
}

internal fun <T> catchingTransaction(action: () -> T?): T? {
    return try {
        transaction {
            action()
        }
    } catch (e: Exception) {
        e.printStackTrace()
        null
    }
}
