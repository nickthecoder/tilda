package uk.co.nickthecoder.tilda

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.routing.Route
import io.ktor.routing.get
import uk.co.nickthecoder.tilda.model.Song
import uk.co.nickthecoder.tilda.player.player

internal fun Route.routeSong() {
    get( "view" ) {
        call.requiredIntParameter("songId")?.let { songId ->
            call.viewSong(call.viewType(), songId)
        }
    }
}

private suspend fun ApplicationCall.viewSong( viewType: ViewType, songId : Int ) {
    findOrError(viewType, "Song") {
        Song.findById(songId)
    }?.let { song ->

        if (viewType == ViewType.JSON) {
            respondJson(
                mapOf(
                        "songId" to song.id.value,
                        "albumId" to song.album.id.value,
                        "artistId" to song.album.artist.id.value,
                        "songName" to song.name,
                        "artistName" to song.album.artist.name,
                        "albumName" to song.album.name,
                        "trackNumber" to song.trackNumber,
                        "seconds" to song.seconds,
                        "path" to song.path
                )
            )
        } else {
            song.lazyAlbum.lazyArtist

            // NOTE, this template doesn't exist, as this request is only used for JSON at the moment.
            val fm = FreeMarkerContent(
                    if (viewType == ViewType.PRETTY) "song-pretty.ftlh" else "song.ftlh",
                    mapOf(
                            "song" to song,
                            "player" to player,
                            "search" to Controller.search
                    ),
                    "e"
            )

            respondFreemarker(fm)
        }
    }
}
