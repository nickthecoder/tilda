package uk.co.nickthecoder.tilda

import org.jetbrains.exposed.sql.SortOrder
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import uk.co.nickthecoder.tilda.model.*
import uk.co.nickthecoder.tilda.player.PlayerListener
import uk.co.nickthecoder.tilda.player.PlayerState
import uk.co.nickthecoder.tilda.player.player
import java.io.File
import java.util.logging.Level

private fun searchFromPath(path: String?): Search? {

    return if (path == null || path.isBlank()) {
        null
    } else {
        try {
            val dir = File(path)
            if (dir.isDirectory || dir.mkdirs()) {
                Search(File(path))
            } else {
                null
            }
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }
}

object Controller {

    private val listener = object : PlayerListener {
        override fun stateChanged(oldState: PlayerState, newState: PlayerState) {
            if (oldState == PlayerState.PLAYING && newState == PlayerState.IDLE) {
                player.latestFile?.let {
                    transaction { finishedPlaying(it, true) }
                }
            }
        }
    }

    val search = searchFromPath(transaction { KnownSettings.LUCENE_DIRECTORY.find()?.value })

    fun begin() {
        player.addListener(listener)
    }

    fun play() {
        when (player.playerState) {
            PlayerState.PAUSED -> player.unPause()
            else -> {
                val playlist = Playlist.findCurrent()
                val entry = playlist.lazyEntries.firstOrNull()
                if (entry == null) {
                    randomPlay()
                } else {
                    playlist.position = entry.orderNumber
                    entry.song.lastPlayed = DateTime.now()
                    player.play(entry.song.file)
                }
            }
        }
    }

    fun skip() {
        Tilda.logger.log(Level.FINE, "Skip")
        player.latestFile?.let {
            player.stop()
            finishedPlaying(it, KnownSettings.REMOVE_ON_SKIP.isTrue())
        }
    }

    /**
     * Called when we want to move to the next item in the playlist, or pick a random song
     * if there are no more playlist entries and the [KnownSettings.RANDOM_PLAY] setting is true.
     *
     * In some cases we want to delete the entry for the current playlist :
     *
     * * If this is called when a playlist entry finishes normally
     * * If we have skipped, and [KnownSettings.REMOVE_ON_SKIP] setting is true
     * * If we have played a new song and [KnownSettings.REMOVE_ON_PLAY_OTHER] setting is true
     */
    private fun finishedPlaying(file: File, remove: Boolean) {
        Tilda.logger.log(Level.FINE, "Finished playing $file remove? $remove")

        findEntry(file)?.let { entry ->

            if (!startNextSong(entry)) {

            }

            if (remove) {
                if (!entry.playlist.permanent) {
                    removeEntry(entry)
                }
            }

        }
    }

    fun currentEntry(): Entry? = findEntry(player.latestFile)

    private fun findEntry(file: File?): Entry? {
        file ?: return null
        return Playlist.findEntry(file)
    }

    private fun findNext(entry: Entry?): Entry? {
        entry ?: return null

        return entry.playlist.entries.firstOrNull { it.orderNumber == entry.orderNumber + 1 }
    }

    private fun removeEntry(entry: Entry?) {
        entry ?: return
        Tilda.logger.log(Level.FINE, "Deleting entry for song : ${entry.song.name}")

        val playlist = entry.playlist
        for (other in playlist.entries) {
            if (other.orderNumber > entry.orderNumber) {
                other.orderNumber--
            }
        }
        if (playlist.position > entry.orderNumber) {
            playlist.position--
        }

        entry.delete()

    }

    private fun randomPlay(reshuffle: Boolean = true) {

        Songs.slice(Songs.id, Songs.randomOrder)
                .selectAll()
                .orderBy(Songs.randomOrder, SortOrder.DESC)
                .limit(1).firstOrNull()
                ?.let { row ->

                    val songId = row[Songs.id]
                    val randomOrder = row[Songs.randomOrder]

                    if (randomOrder >= 0) {

                        Tilda.logger.log(Level.FINE, "Found song : ${songId}")
                        Song.findById(songId)?.let { song ->
                            song.randomOrder = -1
                            Tilda.logger.log(Level.FINE, "Random play ${song.name}")
                            addSong(song, false)
                        }
                    } else {
                        // There are no more songs left with a positive randomOrder
                        if (reshuffle) {
                            Tilda.logger.log(Level.INFO, "Reshuffling")
                            Song.all().forEach { song ->
                                song.resetRandomOrder()
                            }
                            randomPlay(false) // False prevents infinite recursion.
                        }
                    }

                    return
                }
    }

    /**
     * Removes the [entry], and if that entry was the currently playing song, then move onto the next song
     * which is either the next entry in the playlist, or a random song if the playlist is empty and
     * [KnownSettings.RANDOM_PLAY] is true.
     */
    fun removeEntryAndContinue(entry: Entry?) {
        entry ?: return

        val playlist = entry.playlist
        val wasPlaying = playlist.current && playlist.position == entry.orderNumber

        if (playlist.permanent) {
            Tilda.logger.log(Level.FINE, "This is a permanent playlist, so not deleting the entry")
        } else {
            removeEntry(entry)
        }

        if (wasPlaying) {
            startNextSong(entry)
        }
    }

    private fun startNextSong(current: Entry?): Boolean {
        val playlist = current?.playlist
        val next = current?.let { findNext(current) }

        if (next == null) {
            playlist?.position = 1
            // We have got to the end of the playlist, but if we started in the MIDDLE, then
            // we need to start again from the top of the list.
            playlist?.entries?.firstOrNull { it.orderNumber == 1 && it !== current }?.let { firstEntry ->
                if (playlist.loop) {
                    Tilda.logger.log(Level.FINE, "Playing first song in playlist ${firstEntry.song.name}")
                    firstEntry.song.lastPlayed = DateTime.now()
                    player.play(firstEntry.song.file)
                    return true
                }
            }
        } else {
            Tilda.logger.log(Level.FINE, "Playing next song in playlist ${next.song.name}")
            next.song.lastPlayed = DateTime.now()
            player.play(next.song.file)
            return true
        }

        Tilda.logger.log(Level.FINE, "Nothing in the playlist, so maybe random play?")
        if (KnownSettings.RANDOM_PLAY.isTrue()) {
            randomPlay()
        }
        return false
    }

    fun addSong(song: Song, queue: Boolean): Entry {
        val playlist = Playlist.findCurrent()
        return addSong(playlist, song, queue)
    }

    fun addSong(playlist: Playlist, song: Song, queue: Boolean): Entry {

        val count = playlist.entries.count()

        return if (queue) {
            insertEntry(playlist, song, count + 1, false)
        } else {
            val playNow = !KnownSettings.PLAY_WAITS.isTrue() || !player.isPlaying()
            if (count == 0) {
                insertEntry(playlist, song, 1, playNow)
            } else {
                insertEntry(playlist, song, playlist.position + 1, playNow)
            }
        }
    }

    fun addAlbum(album: Album, queue: Boolean, clear: Boolean) {
        val playlist = Playlist.findCurrent()
        addAlbum(playlist, album, queue, clear)
    }

    fun addAlbum(playlist: Playlist, album: Album, queue: Boolean, clear: Boolean) {
        if (clear) {
            playlist.entries.forEach { it.delete() }
            playlist.position = -1
        }

        val count = playlist.entries.count()
        var play = false
        var orderNumber = if (queue && !clear) {
            count + 1
        } else {
            val wait = KnownSettings.PLAY_WAITS.isTrue()
            if (!wait || count == 0) {
                play = true
                1
            } else {
                playlist.position + 1
            }
        }
        album.lazySongs.forEach { song ->
            insertEntry(playlist, song, orderNumber, play)
            play = false
            orderNumber++
        }
    }

    fun insertEntry(playlist: Playlist, song: Song, orderNumber: Int, play: Boolean = false): Entry {

        playlist.entries.forEach { entry ->
            if (entry.orderNumber >= orderNumber) {
                entry.orderNumber++
            }
        }
        val entry = Entry.new {
            this.playlist = playlist
            this.song = song
            this.orderNumber = orderNumber
        }
        if (play) {
            playlist.position = orderNumber
            song.lastPlayed = DateTime.now()
            player.play(song.file)
        }
        return entry
    }

    fun playEntry(entry: Entry) {
        player.latestFile?.let { file ->
            if (KnownSettings.REMOVE_ON_PLAY_OTHER.isTrue()) {
                findEntry(file)?.let { current ->
                    if (!current.playlist.permanent) {
                        removeEntry(current)
                    }
                }
            }
        }

        entry.playlist.position = entry.orderNumber
        entry.song.lastPlayed = DateTime.now()
        player.play(entry.song.file)
    }
}
