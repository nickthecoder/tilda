package uk.co.nickthecoder.tilda

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveParameters
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import org.jetbrains.exposed.sql.transactions.transaction
import uk.co.nickthecoder.tilda.model.Album
import uk.co.nickthecoder.tilda.model.Entry
import uk.co.nickthecoder.tilda.model.Playlist
import uk.co.nickthecoder.tilda.model.Song
import uk.co.nickthecoder.tilda.player.player

internal fun Route.routePlaylist() {

    get("list") {
        call.listPlaylists(call.viewType())
    }
    get("view/{playlistId}") {
        call.requiredIntParameter("playlistId")?.let { playlistId ->
            call.viewPlaylist(call.viewType(), playlistId)
        }
    }
    post("update") {
        val parameters = call.receiveParameters()
        call.requiredIntParameter("playlistId", parameters)?.let { playlistId ->
            call.updatePlaylist(
                    call.viewType(),
                    playlistId,
                    parameters["name"],
                    parameters["loop"] != null,
                    parameters["permanent"] != null
            )
        }
    }
    post("delete") {
        val parameters = call.receiveParameters()
        call.requiredIntParameter("playlistId", parameters)?.let { playlistId ->
            call.deletePlaylist(call.viewType(parameters), playlistId)
        }
    }
    post("select") {
        val parameters = call.receiveParameters()
        call.requiredIntParameter("playlistId", parameters)?.let { playlistId ->
            call.selectPlaylist(call.viewType(parameters), playlistId)
        }
    }
    post("clear") {
        val parameters = call.receiveParameters()
        call.requiredIntParameter("playlistId", parameters)?.let { playlistId ->
            call.clearPlaylist(call.viewType(parameters), playlistId)
        }
    }
    post("new") {
        val parameters = call.receiveParameters()
        val viewType = call.viewType(parameters)

        transaction {
            Playlist.new {
                name = "New Playlist"
                current = false
                permanent = false
                loop = true
                position = 0
            }
        }
        call.respondOk(viewType)
    }

    post("addSong") {
        val parameters = call.receiveParameters()
        val queue = parameters["play"] == null
        val viewType = call.viewType(parameters)

        call.requiredIntParameter("songId", parameters)?.let { songId ->
            catchingTransaction {
                Song.findById(songId)?.let { song ->
                    Controller.addSong(song, queue)
                }
            }
            call.respondOk(viewType)
        }
    }

    post("addAlbum") {
        val parameters = call.receiveParameters()
        val viewType = call.viewType(parameters)
        val queue = parameters["play"] == null
        val clear = parameters["clear"] != null

        call.requiredIntParameter("albumId", parameters)?.let { albumId ->
            catchingTransaction {
                Album.findById(albumId)?.let { album ->
                    Controller.addAlbum(album, queue, clear)
                }
            }
            call.respondOk(viewType)
        }
    }
    post("playEntry") {
        val parameters = call.receiveParameters()
        val viewType = call.viewType(parameters)

        call.requiredIntParameter("entryId", parameters)?.let { entryId ->
            catchingTransaction {
                Entry.findById(entryId)?.let {
                    Controller.playEntry(it)
                }
            }
            call.respondOk(viewType)
        }
    }
    post("removeEntry") {
        val parameters = call.receiveParameters()
        val viewType = call.viewType(parameters)

        call.requiredIntParameter("entryId", parameters)?.let { entryId ->
            catchingTransaction {
                Entry.findById(entryId)?.let {
                    Controller.removeEntryAndContinue(it)
                }
            }
            call.respondOk(viewType)
        }
    }
}

private suspend fun ApplicationCall.listPlaylists(viewType: ViewType) {
    findOrError(viewType, "Playlists") {
        Playlist.all().toList()

    }?.let { playlists ->
        if (viewType == ViewType.JSON) {
            respondJson(
                    playlists.map { playlist ->
                        mapOf(
                                "playlistId" to playlist.id.value,
                                "name" to playlist.name,
                                "loop" to playlist.loop,
                                "permanent" to playlist.permanent
                        )
                    }
            )

        } else {
            val fm = FreeMarkerContent("playlists.ftlh", mapOf(
                    "playlists" to playlists,
                    "player" to player,
                    "search" to Controller.search
            ), "e")
            respondFreemarker(fm)
        }
    }
}

private suspend fun ApplicationCall.viewPlaylist(viewType: ViewType, playlistId: Int) {
    findOrError(viewType, "Playlist") {
        transaction {
            Playlist.findById(playlistId)?.apply {
                lazyEntries.forEach { it.lazySong.lazyAlbum.lazyArtist }
            }
        }
    }?.let { playlist ->
        val fm = FreeMarkerContent(
                "playlist.ftlh",
                mapOf(
                        "playlist" to playlist,
                        "player" to player,
                        "search" to Controller.search
                ),
                "e"
        )
        respondFreemarker(fm)
    }
}

private suspend fun ApplicationCall.updatePlaylist(
        viewType: ViewType,
        playlistId: Int,
        name: String?,
        loop: Boolean,
        permanent: Boolean) {
    transaction {
        Playlist.findById(playlistId)?.let { playlist ->
            name?.let { playlist.name = it }
            playlist.loop = loop
            playlist.permanent = permanent
        }
    }
    respondOk(viewType)
}

private suspend fun ApplicationCall.deletePlaylist(viewType: ViewType, playlistId: Int) {

    transaction {
        Playlist.findById(playlistId)?.let { playlist ->
            if (!playlist.current) {
                playlist.delete()
            }
        }
    }
    respondOk(viewType)
}

private suspend fun ApplicationCall.selectPlaylist(viewType: ViewType, playlistId: Int) {

    transaction {
        Playlist.all().forEach { playlist ->
            playlist.current = playlist.id.value == playlistId
        }
    }

    respondOk(viewType)
}

private suspend fun ApplicationCall.clearPlaylist(viewType: ViewType, playlistId: Int) {

    val found = transaction {
        val playlist = Playlist.findById(playlistId)
        if (playlist == null) {
            false
        } else {
            playlist.entries.forEach { it.delete() }
            true
        }
    }
    if (found) {
        respondOk(viewType)
    } else {
        respondError(viewType, "Playlist not found", HttpStatusCode.NotFound)
    }
}
