package uk.co.nickthecoder.tilda

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.http.CacheControl
import io.ktor.response.cacheControl
import io.ktor.response.respondFile
import io.ktor.response.respondRedirect
import io.ktor.routing.Route
import io.ktor.routing.get
import uk.co.nickthecoder.tilda.model.Album
import uk.co.nickthecoder.tilda.player.player

internal fun Route.routeAlbum() {

    get("view/{albumId}") {
        call.requiredIntParameter("albumId")?.let { albumId ->
            call.album(call.viewType(), albumId)
        }
    }
    get("cover/{albumId}") {
        call.requiredIntParameter("albumId")?.let { albumId ->
            call.albumCover(albumId)
        }
    }
}

internal suspend fun ApplicationCall.albums(viewType: ViewType) {
    findOrError(viewType, "Album") {
        Album.all().sortedBy { it.name }
    }?.let { albums ->

        if (viewType == ViewType.JSON) {
            albums.map{ album ->
                mapOf<String, Any>(
                        "artistId" to album.artist.id.value,
                        "artistName" to album.artist.name,

                        "albumId" to album.id.value,
                        "albumName" to album.name
                )
            }
        } else {
            val fm = FreeMarkerContent( "albums.ftlh", mapOf(
                    "albums" to albums,
                    "player" to player,
                    "search" to Controller.search
            ), "e")

            respondFreemarker(fm)
        }
    }
}

internal suspend fun ApplicationCall.album(viewType: ViewType, albumId: Int) {
    findOrError(viewType, "Album") {
        Album.findById(albumId)?.apply {
            lazySongs
            lazyArtist
        }
    }?.let { album ->

        if ( viewType == ViewType.JSON ) {
            respondJson(
                    mapOf(
                            "albumId" to album.id.value,
                            "albumName" to album.name,
                            "artistId" to album.artist.id.value,
                            "artistName" to album.artist.name,
                            "songs" to album.lazySongs.map{ song->
                                mapOf(
                                        "songId" to song.id.value,
                                        "songName" to song.name,
                                        "trackNumber" to song.trackNumber,
                                        "seconds" to song.seconds,
                                        "path" to song.path                                )
                            }
                    )
            )
        } else {
            val fm = FreeMarkerContent(
                    if (viewType == ViewType.PRETTY) "album-pretty.ftlh" else "album.ftlh",
                    mapOf(
                            "album" to album,
                            "player" to player,
                            "search" to Controller.search
                    ),
                    "e"
            )

            respondFreemarker(fm)
        }
    }
}

internal suspend fun ApplicationCall.albumCover( albumId: Int) {
    val album = catchingTransaction {
        Album.findById(albumId)
    }
    val file = album?.imageFile
    if (file?.exists() == true) {
        response.cacheControl(CacheControl.MaxAge(60 * 60))
        respondFile(file)
    } else {
        respondRedirect("/skin/noAlbumCover.jpg")
    }
}
