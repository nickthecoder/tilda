package uk.co.nickthecoder.tilda.model

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object Entries : IntIdTable(name = "entry") {

    val playlist = reference("playlist", Playlists)
    val song = reference("song", Songs)

    val orderNumber = integer("orderNumber")
}

class Entry(id: EntityID<Int>) : IntEntity(id), Comparable<Entry> {

    companion object : IntEntityClass<Entry>(Entries)

    var playlist by Playlist referencedOn Entries.playlist

    val lazyPlaylist by lazy { playlist }

    var song by Song referencedOn Entries.song

    val lazySong by lazy { song }

    /**
     * The index of this playlist entry. The first entry is 1, then 2 etc.
     */
    var orderNumber by Entries.orderNumber

    override fun compareTo(other: Entry): Int {
        return orderNumber - other.orderNumber
    }
}
