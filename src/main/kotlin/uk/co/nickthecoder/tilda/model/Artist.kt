package uk.co.nickthecoder.tilda.model

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable


object Artists : IntIdTable(name = "artist") {
    val name = varchar("name", 100).index()
    val available = bool("available")
}

class Artist(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Artist>(Artists)

    var name by Artists.name
    var available by Artists.available

    val albums by Album referrersOn Albums.artist

    val lazyAlbums: List<Album> by lazy {
        albums.filter { it.available }.sortedBy { it.name }
    }
}
