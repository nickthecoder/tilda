package uk.co.nickthecoder.tilda.model

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.joda.time.DateTime
import uk.co.nickthecoder.tilda.humanTime
import java.io.File
import java.util.*

object Songs : IntIdTable(name = "song") {
    val name = varchar("name", 100).index()
    val trackNumber = integer("trackNumber")

    val seconds = integer("seconds")

    val path = varchar("path", 500)
    val scanDate = datetime("scanDate")
    val modifiedDate = datetime("modifiedDate")
    val lastPlayed = datetime("lastPlayed").nullable()
    val available = bool("available")
    val randomOrder = integer("randomOrder").index()

    val album = reference("album", Albums)
}

class Song(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Song>(Songs)

    /**
     * The track number within the album. 1 is the first track.
     */
    var trackNumber by Songs.trackNumber

    /**
     * The duration of the song in seconds.
     */
    var seconds by Songs.seconds

    val time: String
        get () = humanTime(seconds)


    /**
     * The name of the song. This is taken from the song's title tag, and if not found, then
     * it is derived from the file's path (stripped of extension and a track number prefix if there is one).
     */
    var name by Songs.name

    /**
     * The full path of the song. See [file].
     */
    var path by Songs.path

    /**
     * The file for this song. This is derived from [path].
     * These should be absolute paths; never relative.
     */
    var file: File
        get() = File(path)
        set(v) {
            path = v.absolutePath
        }

    /**
     * The date and time that the song's meta-data was extracted.
     */
    var scanDate: DateTime by Songs.scanDate

    /**
     * The data and time that this song was last updated in the database.
     */
    var modifiedDate by Songs.modifiedDate

    /**
     * The date and time that this song was last played.
     */
    var lastPlayed by Songs.lastPlayed

    val lastPlayedString : String
        get() {
            val lp = lastPlayed ?: return ""
            val now = DateTime.now()
            return if (now.year == lp.year) {
                if (now.dayOfYear() == lp.dayOfYear()) {
                    lp.toString( "HH:mm")
                } else {
                    lp.toString("YYYY-MM-dd")
                }
            } else {
                lp.toString("YYYY-MM-dd")
            }
        }

    /**
     * If the song's file doesn't exist.
     * False if the file has been deleted, or its file system has been unmounted.
     */
    var available by Songs.available

    /**
     * A random number is given to all new Songs.
     * When a random track is requested the song with the largest randomOrder is chosen,
     * and it's randomOrder is reset to -1.
     * If there are no more songs with positive randomOrders, then all songs are re-assigned
     * a randomOrder.
     * When this song is played randomOrder is set to -1 (regardless of whether it was picked randomly, or
     * chosen by the user).
     * In this way, random play will not repeat a song until, nor miss some tracks.
     */
    var randomOrder by Songs.randomOrder


    var album by Album referencedOn Songs.album

    val lazyAlbum by lazy { album }

    fun resetRandomOrder() {
        randomOrder = Random().nextInt(Int.MAX_VALUE)
    }
}
