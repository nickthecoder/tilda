package uk.co.nickthecoder.tilda.model

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import java.io.File

object Playlists : IntIdTable(name = "playlist") {
    val name = varchar("name", 100)
    val current = bool("current")
    val permanent = bool("permanent")
    val loop = bool("loop")

    val position = integer("position")
}

class Playlist(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<Playlist>(Playlists) {
        fun findCurrent(): Playlist {
            return find { Playlists.current eq true }.first()
        }

        fun findEntry(file: File): Entry? {
            val path = file.path
            return findCurrent().entries.firstOrNull { it.song.path == path }
        }
    }

    var name by Playlists.name

    var current by Playlists.current

    /**
     * Entries in permanent playlists are NOT removed when the song finishes playing.
     */
    var permanent by Playlists.permanent

    /**
     * When we get to the end of the playlist, should we jump back to the start?
     * If loop and permanent are both set, then this gives a never ending playlist.
     */
    var loop by Playlists.loop

    /**
     * The order number Entry currently being played (or next to be played if idle)
     * When playing from the top of the playlist for a non-permanent playlist, this
     * will always be 1.
     */
    var position by Playlists.position

    val entries by Entry referrersOn Entries.playlist

    val lazyEntries by lazy { entries.toSortedSet() }

}
