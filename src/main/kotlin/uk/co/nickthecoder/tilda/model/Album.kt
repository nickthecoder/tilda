package uk.co.nickthecoder.tilda.model

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import java.io.File

object Albums : IntIdTable(name = "album") {
    val name = varchar("name", 100).index()
    val available = bool("available")
    val imagePath = varchar("imagePath", 500).nullable()

    val artist = reference("artist", Artists)
}

class Album(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Album>(Albums)

    var name by Albums.name

    var available by Albums.available

    var imagePath by Albums.imagePath

    var imageFile: File?
        get() = imagePath?.let { File(it) }
        set(v) {
            imagePath = if (v == null) null else v.absolutePath
        }

    var artist by Artist referencedOn Albums.artist

    val lazyArtist by lazy { artist }

    val songs by Song referrersOn Songs.album

    val lazySongs by lazy { songs.filter { it.available }.sortedBy { it.trackNumber } }
}
