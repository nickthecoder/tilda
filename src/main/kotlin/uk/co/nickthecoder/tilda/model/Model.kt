package uk.co.nickthecoder.tilda.model

import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.transaction

object Model {

    val tables = arrayOf(Artists, Albums, Songs, Settings, Playlists, Entries)

    fun rebuildDatabase() {

        // Remember the old settings
        val settings = transaction {
            Setting.all().toList()
        }

        transaction { SchemaUtils.drop(* tables) }

        createSchema()

        // Copy the old settings. Any extra settings in old will be ignored (only KnownSettings will be updated).
        transaction {
            settings.forEach { old ->
                Setting.findById(old.id.value)?.let { new ->
                    new.value = old.value
                }
            }
        }
    }

    fun createSchema() {

        transaction {
            addLogger(StdOutSqlLogger)
            SchemaUtils.create(* tables)
        }
        Settings.addMissingOptions()

        // Make sure there is a single current playlist.
        transaction {
            val playlists = Playlist.all()

            if (playlists.empty()) {
                Playlist.new {
                    name = "Default"
                    current = true
                    permanent = false
                    loop = true
                    position = 1
                }
            } else {
                val count = playlists.filter { it.current }.count()
                if (count != 1) {
                    var found = false
                    playlists.forEach { playlist ->
                        if (found) {
                            found = false
                            playlist.current = false
                        } else {
                            if (count == 0 || playlist.current) {
                                playlist.current = true
                                found = true
                            }
                        }
                    }
                }
                Unit
            }
        }
    }

}
