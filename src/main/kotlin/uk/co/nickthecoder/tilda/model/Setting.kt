package uk.co.nickthecoder.tilda.model

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IdTable
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File

enum class KnownSettings(val value: String) {
    MUSIC_PATH(File("/home/nick/music").absolutePath),
    SERVER_PORT("8080"),
    PLAY_WAITS("1"),
    RANDOM_PLAY("1"),
    SKIN("default"),
    SEARCH_DIRECTORY(""),
    PLAY_ON_STARTUP("1"),
    REMOVE_ON_SKIP("0"), // Remove a part-played song when skip is pressed?
    REMOVE_ON_PLAY_OTHER("0"), // If a new track is played part way through a song, remove the old song from the playlist?
    LUCENE_DIRECTORY(""),
    SHOW_QUIT("0");

    fun find() = Setting.findById(name)
    fun isTrue(default: Boolean = true) = find()?.booleanValue ?: default
}

object Settings : IdTable<String>(name = "setting") {
    override val id = varchar("id", 30).primaryKey().entityId()
    val value = varchar("value", 100)

    fun addMissingOptions() {
        transaction {
            for (defaultOption in KnownSettings.values()) {
                if (defaultOption.find() == null) {
                    Setting.new(defaultOption.name) {
                        value = defaultOption.value
                    }
                }
            }
        }
    }

}

class Setting(id: EntityID<String>) : Entity<String>(id) {

    companion object : EntityClass<String, Setting>(Settings)

    var value by Settings.value

    var booleanValue: Boolean
        get() = value == "1"
        set(v) {
            value = if (v) "1" else "0"
        }

    var intValue: Int
        get() = value.toInt()
        set(v) {
            value = v.toString()
        }
}
