package uk.co.nickthecoder.tilda

import java.io.File
import java.lang.reflect.Field
import java.lang.reflect.Method


val NULL_FILE = File(
        if (System.getProperty("os.name").startsWith("Windows")) "NUL" else "/dev/null"
)

/**
 * Returns the PID of this process.
 * When using Java9 + it uses the now standard Process.pid() method,
 * but in Java 8 -, it uses reflection to get the private pid field on
 * UnixProcess (the sub-class of Process used on Unix-like systems).
 *
 * Note, this returns a Long, rather than an Int because the Java9 API uses a long.
 */
fun Process.unixPID(): Long? {
    try {
        // Java 9 now has a getPid method, so try that first
        val pidMethod: Method = Process::class.java.getMethod("pid")
        return pidMethod.invoke(this) as Long

    } catch (e: Exception) {
    }

    try {
        val pidField: Field = this.javaClass.getDeclaredField("pid")
        pidField.isAccessible = true
        return (pidField.get(this) as Int?)?.toLong()

    } catch (e: Exception) {
    }

    return null
}

fun Process.signal(signalNumber: Int) {
    unixPID()?.let { pid ->
        ProcessBuilder("kill", "-$signalNumber", "$pid").apply {
            redirectError(NULL_FILE)
            redirectOutput(NULL_FILE)
            redirectInput(NULL_FILE)
        }.start()
    }
}

// Here are the three most command signals that I use.
fun Process.sigint() = signal(1)

fun Process.sigstop() = signal(19)
fun Process.sigcont() = signal(18)
fun Process.sigkill() = signal(9)

