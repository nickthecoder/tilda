package uk.co.nickthecoder.tilda

import org.jaudiotagger.audio.AudioFileIO
import org.jaudiotagger.tag.FieldKey
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import uk.co.nickthecoder.tilda.model.*
import java.io.File
import java.util.logging.Level
import java.util.logging.Logger

enum class ScannerState { IDLE, SCANNING }

object Scanner {

    val musicExtensions = mutableListOf("mp3", "wav", "flac", "ogg")

    val imageExtensions = mutableListOf("jpg", "png", "gif")

    val preferedImages = mutableListOf(
            File(".meta${File.separator}cover_400.jpg"),
            File("cover.jpg")
    )

    private var state: ScannerState = ScannerState.IDLE

    // Ensure that the logger isn't gc'd, otherwise setting Level.OFF may be lost.
    // See https://stackoverflow.com/questions/40923754/selenium-many-logs-how-to-remove/40934452#40934452
    private val jaudioLogger = Logger.getLogger("org.jaudiotagger")

    private var searchWriter: Search.Writer? = null

    private var privateLatestDir: File? = null

    val latestDir: File?
        get() = privateLatestDir

    init {
        jaudioLogger.level = Level.OFF
    }

    fun isScanning(): Boolean = state == ScannerState.SCANNING

    fun scan(purge: Boolean = false) {

        if (state == ScannerState.IDLE) {
            state = ScannerState.SCANNING
            Thread {
                try {
                    Tilda.logger.log(Level.INFO, "Starting a scan")
                    doScan()
                    if (purge) {
                        Tilda.logger.log(Level.INFO, "purging")
                        purge()
                    }
                    Tilda.logger.log(Level.INFO, "Scan completed normally")

                    Controller.search?.let { search ->
                        Tilda.logger.log(Level.INFO, "Lucene rebuild started")
                        search.rebuild()
                        Tilda.logger.log(Level.INFO, "Lucene rebuild ended")
                    }

                } finally {
                    Tilda.logger.log(Level.INFO, "Scan ended")
                    state = ScannerState.IDLE
                }
            }.start()
        }
    }

    private fun doScan() {
        searchWriter = Controller.search?.open()
        try {
            val path = transaction {
                KnownSettings.MUSIC_PATH.find()
            }
            if (path != null) {

                // Scan each directory for music
                val musicPath = path.value
                musicPath.split(File.pathSeparator).forEach { dir ->
                    scanDirectory(File(dir))
                }

                // Look for images for each album
                transaction {
                    Album.all().forEach { album ->
                        var directory: File? = null
                        album.songs.forEach { song ->
                            val songDir = song.file.parentFile
                            if (songDir != directory) {
                                directory = songDir
                                findImage(songDir)?.let {
                                    album.imageFile = it
                                }

                            }
                        }
                    }
                }
            }
            // Now check every song, to see if the file exists, and tag it as not "available" it if it doesn't.
            // Do one artists at a time, so that there isn't too many items per transaction.
            transaction {
                Artist.all().toList()
            }.forEach { fromList ->
                transaction {
                    Artist.findById(fromList.id.value)?.let { artist ->
                        var hasAlbum = false
                        artist.albums.forEach { album ->
                            var hasSong: Boolean = false
                            album.songs.forEach { song ->
                                song.available = song.file.exists()
                                hasSong = hasSong || song.available
                                if (!song.available) {
                                    searchWriter?.deleteSong(song)
                                }
                            }
                            album.available = hasSong
                            hasAlbum = hasAlbum || album.available
                            if (!album.available) {
                                searchWriter?.deleteAlbum(album)
                            }
                        }
                        artist.available = hasAlbum
                        if (!artist.available) {
                            searchWriter?.deleteArtist(artist)
                        }
                    }
                }
            }
        } catch (e: Exception) {
            Tilda.logger.log(Level.SEVERE, "Scan failed")
            e.printStackTrace()
        } finally {
            searchWriter?.close()
        }

    }

    /**
     * Permanently deletes database rows where "available" is false.
     * Therefore any songs/artist/albums that have been deleted from the file system,
     * will be removed from the database also.
     * Without a purge, the rows would only be marked as not "available" by the scan.
     * The user interface would look the same (but may be a little slower wothout a purge).
     */
    private fun purge() {
        transaction {
            Song.find { Songs.available eq false }.forEach { it.delete() }
        }
        transaction {
            Album.find { Albums.available eq false }.forEach { it.delete() }
        }
        transaction {
            Artist.find { Artists.available eq false }.forEach { it.delete() }
        }
    }

    /**
     * Looks for an image for this album.
     */
    private fun findImage(directory: File): File? {
        for (f in preferedImages) {
            val file = File(directory, f.path)
            if (file.exists()) return file
        }
        directory.listFiles()?.let {
            it.forEach { child ->
                if (imageExtensions.contains(child.extension.toLowerCase())) {
                    return child
                }
            }
        }
        return null
    }

    private fun scanDirectory(dir: File) {
        Tilda.logger.log(Level.INFO, "Scanning $dir")

        privateLatestDir = dir
        dir.listFiles()?.let { filesAndDirs ->

            val files = filesAndDirs.filter { it.isFile }
            if (files.isNotEmpty()) {
                transaction {
                    files.forEach { child ->
                        val ext = child.extension.toLowerCase()
                        if (musicExtensions.contains(ext)) {
                            scanFile(child)
                        }
                    }
                }
            }

            filesAndDirs.filter { it.isDirectory && !it.isHidden }.sortedBy { it.name }.forEach {
                scanDirectory(it)
            }
        }
    }

    private fun scanFile(file: File) {

        val now = DateTime.now()
        val fileDate = file.lastModified()

        var song = Song.find { Songs.path eq file.path }.firstOrNull()
        if (song != null && song.scanDate.isAfter(fileDate)) {
            // No need to re-scan it
            song.available = true
            song.modifiedDate = now
            return
        }


        val f = try {
            AudioFileIO.read(file)
        } catch (e: Exception) {
            Tilda.logger.log(Level.INFO, "Skipping $file as it cannot be read.")
            song?.available = false
            song?.modifiedDate = now
            return
        }
        val tag = f.tag
        val header = f.audioHeader

        val trackLength = default(0) { header.trackLength }
        val trackNumber = default(0) { tag.getFirst(FieldKey.TRACK).toInt() }

        val artistName = default(guessArtistName(file)) { tag.getFirst(FieldKey.ARTIST) }
        val albumName = default(guessAlbumName(file)) { tag.getFirst(FieldKey.ALBUM) }
        val songName = default(guessSongName(file)) { tag.getFirst(FieldKey.TITLE) }

        val album = findOrCreateAlbum(artistName, albumName)

        if (song == null) {
            song = Song.new {}
        }

        with(song) {
            this.album = album
            this.name = songName.maxLength(100)
            this.trackNumber = trackNumber
            seconds = trackLength
            this.file = file
            modifiedDate = now
            scanDate = now
            available = true
            resetRandomOrder()
        }

        searchWriter?.addSong(song)
        searchWriter?.commit()

    }

    private fun findOrCreateAlbum(artistName: String, albumName: String): Album {

        var artist = Artist.find { Artists.name eq artistName }.firstOrNull()
        if (artist == null) {
            artist = Artist.new {
                name = artistName.maxLength(100)
                available = true
            }
            searchWriter?.addArtist(artist)
        }

        var album = artist.albums.firstOrNull { it.name == albumName }
        if (album == null) {
            album = Album.new {
                this.artist = artist
                name = albumName.maxLength(100)
                available = true
                imagePath = null
            }
            searchWriter?.addAlbum(album)
        }

        return album
    }

    private fun guessArtistName(file: File): String {
        return file.parentFile.parent
    }

    private fun guessAlbumName(file: File): String {
        return file.parent
    }

    private fun guessSongName(file: File): String {
        return file.nameWithoutExtension
    }
}

private fun <T> default(default: () -> T, value: () -> T): T {
    return try {
        value()
    } catch (e: Exception) {
        default()
    }
}

private fun <T> default(default: T, value: () -> T): T {
    return try {
        value()
    } catch (e: Exception) {
        default
    }
}

private fun String.maxLength(l: Int, suffix: String = "…"): String {
    return if (this.length > l) {
        return this.substring(0, l - suffix.length) + suffix
    } else {
        this
    }
}
