package uk.co.nickthecoder.tilda

enum class ViewType {
    NORMAL,
    PRETTY,
    JSON;

    companion object {
        fun fm(pretty: Boolean) = if (pretty) PRETTY else NORMAL
    }
}
